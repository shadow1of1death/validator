# validator

## Модуль валидации данных

Модуль используется для упрощенной валидации и справления ошибок валидации если это неообходимо.
Модуль также структурирует логику валидации для более простого расширения функционала валидации.

## Назначения папок

- **Validator** Главная папка модуля
- **Collections** Коллекции
- **DataTypes** Типы данных валидации, в них прописывается логика валидации
- **Facades** Фасады, для более простой работы с модулем
- **Interfaces** Интерфейсы
- **Utils** Инструменты и Хелперы

## Примеры

Для проверки данных на `null` код будет выглядить следующим образом

```php
use Uniforma\Modules\Validator\Facades\Validator

$data = null;

//Будет возвращен объект типа данных валидации Uniforma\Modules\Validator\DataTypes\ArrayValidator
$validator = Validator::getValidator($data, 'array', ['null'])->check();

//Если необходимо исправить ошибку (исправленное значение можно получить, использовав getFixedValue() на результате)
$validatorFix = Validator::getValidator($data, 'array', ['null'], true)->check();
$fixedData = $validatorFix->getFixedValue();

//Если необходимо использовать кастомные данные при исправлении (исправленное значение можно получить, использовав getFixedValue() на результате)
$validatorFixCustom = Validator::getValidator($data, 'array', ['null' => [['customFixKey' => 'customFixValue']]], true)->check();
$customFixedData = $validatorFixCustom->getFixedValue();
```

Если необходимо проверить типы или на пустоту, то за место `null` можно передать `type` или `empty` соответственно  


В валидатор можно передать несколько условий проверок если необходимо
```php
use Uniforma\Modules\Validator\Facades\Validator

$data = null;

//Сначала будет выполнена проверка на null, после чего будет выполнена проверка вложенности массива
$validator = Validator::getValidator($data, 'array', ['null', 'path' => ['p1', 'P2childP1', 'P3childP2']], true)->check();
$data = $validator->getFixedValue();
/**
 * Результат будет следующим:
 * $data = ['p1' => ['P2childP1' => ['P3childP2' => '']]]; 
 */
```