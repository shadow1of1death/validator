<?php

namespace Uniforma\Modules\Validator;

use Uniforma\Modules\Validator\Collections\ConditionItemCollection;
use Uniforma\Modules\Validator\Utils\DataTypeHelper;

/**
 * Главный класс валидации
 */
class Validator
{
    /**
     * @var DataType Объект для валидации определенного типа данных
     */
    protected DataType $dataType;
    /**
     * @var ConditionItemCollection Коллекция правил валидации
     */
    protected ConditionItemCollection $conditionItems;

    /**
     * Устанавливает объект типа данных валидации по строковому коду типа данных<br>
     * <b>Коды определены в Uniforma\Modules\Validator\Utils\DataTypeHelper::getDataTypeList()</b>
     * @param string $dataType строковый тип данных [string, array]
     * @return $this
     */
    public function setDataTypeByCode(string $dataType): Validator
    {
        $this->dataType = DataTypeHelper::getByCode($dataType);
        return $this;
    }

    /**
     * Запуск проверки по установленной коллекции правил валидации
     * @return DataType
     */
    public function check() : DataType
    {
        /**
         * @var ConditionItem $conditionItem
         */
        foreach ($this->conditionItems as $conditionItem){
            $method = $conditionItem->getMethod();
            $parameters = $conditionItem->getParameters();
            if($parameters !== null){
                $this->dataType->$method($parameters);
            }
            else{
                $this->dataType->$method();
            }
        }
        return $this->dataType;
    }

    /**
     * Получить объект валидации типа данных
     * @return DataType
     */
    public function getDataType(): DataType
    {
        return $this->dataType;
    }

    /**
     * Получить коллекцию правил валидации
     * @return ConditionItemCollection
     */
    public function getConditionItems() : ConditionItemCollection
    {
        return $this->conditionItems;
    }

    /**
     * Установить коллекцию правил валидации
     * @param ConditionItemCollection $conditionItemCollection
     * @return $this
     */
    public function setConditionItems(ConditionItemCollection $conditionItemCollection) : Validator
    {
        $this->conditionItems = $conditionItemCollection;
        return $this;
    }
}