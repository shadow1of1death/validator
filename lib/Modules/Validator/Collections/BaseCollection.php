<?php

namespace Uniforma\Modules\Validator\Collections;

use Traversable;
use Uniforma\Modules\Validator\Interfaces\CollectionInterface;
use Uniforma\Modules\Validator\Utils\CollectionHelper;

/**
 * Базовый класс коллекции
 */
class BaseCollection implements \Countable, \IteratorAggregate, \JsonSerializable, CollectionInterface
{
    /**
     * Элементы коллекции
     * @var array
     */
    protected array $items = [];

    /**
     * Тип данных коллекции, если будет установлен, то будет проверяться тип, добовляемого элемента коллекции
     * @var string|null
     */
    protected ?string $type = null;

    /**
     * Получить количество элементов в коллекции
     * @return int
     */
    public function count(): int
    {
        // TODO: Implement count() method.
        return count($this->items);
    }

    /**
     * Получить итератор коллекции
     * @return Traversable
     */
    public function getIterator(): Traversable
    {
        // TODO: Implement getIterator() method.
        return new \ArrayIterator($this->items);
    }

    /**
     * Получить сериализованную коллекцию
     * @return mixed
     */
    public function jsonSerialize(): mixed
    {
        // TODO: Implement jsonSerialize() method.
        return $this->items;
    }

    /**
     * Очистить коллекцию
     * @return void
     */
    public function clear(): void
    {
        $this->items = [];
    }

    /**
     * Скопировать коллекцию
     * @return $this
     */
    public function copy(): BaseCollection
    {
        return clone $this;
    }

    /**
     * Проверить коллекцию на пустоту
     * @return bool
     */
    public function isEmpty(): bool
    {
        return count($this->items) != 0;
    }

    /**
     * Получить коллекцию как массив
     * @return array
     */
    public function toArray(): array
    {
        return $this->items;
    }

    public function push($item) : BaseCollection
    {
        CollectionHelper::checkItemType($this, $item);
        $this->items[] = $item;
        return $this;
    }

    public function add($item, $key) : BaseCollection
    {
        CollectionHelper::checkItemType($this, $item);
        $this->items[$key] = $item;
        return $this;
    }

    public function last() : mixed
    {
        return end($this->items) === false ? null : end($this->items);
    }

    public function first() : mixed
    {
        return $this->items[array_key_first($this->items)];
    }

    public function getType() : ?string
    {
        return $this->type;
    }
}