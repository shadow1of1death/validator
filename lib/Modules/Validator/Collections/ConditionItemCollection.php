<?php

namespace Uniforma\Modules\Validator\Collections;

/**
 * Класс коллекции правил валидации
 */
class ConditionItemCollection extends BaseCollection
{
    protected ?string $type = 'Uniforma\Modules\Validator\ConditionItem';
}