<?php

namespace Uniforma\Modules\Validator\DataTypes;

use Uniforma\Modules\Validator\DataType;
use Uniforma\Modules\Validator\Facades\Validator;
use Uniforma\Modules\Validator\Interfaces\DataTypeInterface;
use Uniforma\Modules\Validator\Utils\DataTypeHelper;

/**
 * Класс валидации данных типа "строка"
 */
class StringValidator extends DataType implements DataTypeInterface
{
    /**
     * Значение, используемое для исправления в случае возникновления ошибки типов
     * @var mixed|string
     */
    protected mixed $fixType = '';

    /**
     * Значение, используемое для исправления в случае возникновления ошибки 'null'
     * @var mixed|string
     */
    protected mixed $fixNull = '';

    public function checkEmpty(): DataType
    {
        if($this->state->getStatus()){
            if($this->baseValue === '')
                $this->state->setError('string is empty', true);
        }
        return $this;
    }

    public function checkNull(?array $fixNull = null): DataType
    {
        $this->fixNull = Validator::getValidator($fixNull[0], 'string', ['type' => [$this->fixNull]], true)
            ->check()
            ->getFixedValue();

        DataTypeHelper::checkNull($this, 'string is null');
        return $this;
    }

    public function checkType(?array $fixType = null): DataType
    {
        if($fixType[0] !== null && gettype($fixType[0]) === 'string'){
            $this->fixType = $fixType[0];
        }

        if($this->state->getStatus() || $this->fixErrors){
            if(!is_string($this->baseValue)){
                $this->state->setError('string is not string', true);
                DataTypeHelper::fixErrors($this, $this->fixType);
            }
        }
        return $this;
    }
}