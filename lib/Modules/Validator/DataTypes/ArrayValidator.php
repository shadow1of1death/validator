<?php

namespace Uniforma\Modules\Validator\DataTypes;

use Uniforma\Modules\Validator\DataType;
use Uniforma\Modules\Validator\Facades\Validator;
use Uniforma\Modules\Validator\Interfaces\DataTypeInterface;
use Uniforma\Modules\Validator\Utils\DataTypeHelper;

/**
 * Класс валидации данных типа "массив"
 */
class ArrayValidator extends DataType implements DataTypeInterface
{
    /**
     * Значение, используемое для исправления в случае возникновления ошибки типов
     * @var mixed|string
     */
    protected mixed $fixType = [];

    /**
     * Значение, используемое для исправления в случае возникновления ошибки 'null'
     * @var mixed|string
     */
    protected mixed $fixNull = [];

    /**
     * Значение, используемое для установки в последнем элементи проверки вложенности массива, если пути существовать не будет
     * @var mixed|string
     */
    protected mixed $fixEndPath = '';

    public function checkType(?array $fixType = null): DataType
    {
        if($fixType[0] !== null && gettype($fixType[0]) === 'array'){
            $this->fixType = $fixType[0];
        }

        if($this->state->getStatus() || $this->fixErrors){
            if(!is_array($this->baseValue)){
                $this->state->setError('array is not array', true);
                DataTypeHelper::fixErrors($this, $this->fixType);
            }
        }
        return $this;
    }

    public function checkNull(?array $fixNull = null): DataType
    {
        $this->fixNull = Validator::getValidator($fixNull[0], 'array', ['type' => [$this->fixNull]], true)
            ->check()
            ->getFixedValue();

        DataTypeHelper::checkNull($this, 'array is null');
        return $this;
    }

    public function checkEmpty(): DataType
    {
        if($this->state->getStatus()){
            if($this->baseValue === []){
                $this->state->setError('array is empty', true);
            }
        }
        return $this;
    }

    /**
     * Установить значение, используемое для установки в последнем элементи проверки вложенности массива
     * @param $fixEndPath
     * @return $this
     */
    public function setFixEndPath($fixEndPath): ArrayValidator
    {
        $this->fixEndPath = $fixEndPath;
        return $this;
    }

    /**
     * Проверить существования вложенности<br>
     * В случае если путь не будет найден и будет установлен флаг исправления ошибок,
     * то путь будет сгенерирован, его финальным значением будет $fixEndPath
     * @param array $parameters одноуровневый массив, указывающий вложенность друг за другом<br>
     * Пример: ['path1', 'path2', 'path3'], что соответствует ['path1' => ['path2' => ['path3' => 'fixEndPathIfNull']]]
     * @return $this
     */
    public function checkPath(array $parameters): ArrayValidator
    {
        $checked = $this->baseValue ?? [];
        $this->checkPathExists($checked, $parameters);
        if($this->fixErrors) {
            $this->fixedValue = $checked;
        }
        return $this;
    }

    /**
     * Рекурсивная проверка пути<br>
     * @param array $target текущая вложенность массива
     * @param array $arPath оставшийся путь для проверки
     * @return void
     */
    protected function checkPathExists(array &$target, array $arPath) : void
    {
        if($this->state->getStatus() || $this->fixErrors){
            $curPath = $arPath[0];

            if(!array_key_exists($curPath, $target)){
                $this->state->setError("path {$curPath} not find", true);

                if($this->fixErrors){
                    if(count($arPath) > 1){
                        $target[$curPath] = [];
                        $this->checkPathExists($target[$curPath], array_slice($arPath, 1));
                    }
                    else
                        $target[$curPath] = $this->fixEndPath;
                }
            }
        }
    }
}