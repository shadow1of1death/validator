<?php

namespace Uniforma\Modules\Validator;

/**
 * Правило валидации
 */
class ConditionItem
{
    /**
     * @var string Код правила валидации (по нему будет сгенерирован метод)
     */
    protected string $code = '';

    /**
     * @var string Метод правила валидации, генерируется из кода
     */
    protected string $method = '';

    /**
     * Массив параметров (если необходим), для обработки правила
     * @var array|null
     */
    protected ?array $parameters = null;

    /**
     * Сгенерировать наименование метода валидации
     * @return void
     */
    protected function genMethod() : void
    {
        $this->method = "check".ucfirst($this->code);
    }

    /**
     * Получить код правила валидации
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * Получить наименование метода правила валидации
     * @return string
     */
    public function getMethod() : string
    {
        return $this->method;
    }

    /**
     * Установить массив параметров правила валидации
     * @param array|null $parameters
     * @return $this
     */
    public function setParameters(?array $parameters = null) : ConditionItem
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * Получить массив параметров правила валидации
     * @return array|null
     */
    public function getParameters() : ?array
    {
        return $this->parameters;
    }

    /**
     * Установить код правила валидации<br>
     * Автоматически генерируется наименование метода правила валидации
     * @param string $code
     * @return $this
     */
    public function setCode(string $code) : ConditionItem
    {
        $this->code = $code;
        $this->genMethod();
        return $this;
    }
}