<?php

namespace Uniforma\Modules\Validator;

use Uniforma\Modules\Validator\Utils\State;

/**
 * Базовый класс для всех, валидируемых типов данных
 */
abstract class DataType
{
    /**
     * @var State Текущее состояние валидации данных
     */
    protected State $state;

    /**
     * @var mixed Исходное, валидируемое значение
     */
    protected mixed $baseValue;

    /**
     * @var mixed Исправленное $baseValue, если при валидации будет установлен флаг исправления $fixErrors
     */
    protected mixed $fixedValue;

    /**
     * @var mixed Значние, которое будет использовано при исправлении ошибки 'null'
     */
    protected mixed $fixNull;

    /**
     * @var mixed Значение, которое будет использованно при исправлении ошибки несоответствия типа данных
     */
    protected mixed $fixType;

    /**
     * @var bool Флаг, указывающий о необходимости исправления ошибок данных
     */
    protected bool $fixErrors = false;

    public function __construct()
    {
        $this->state = new State();
    }

    /**
     * Получить текущий статус валидации
     * @return bool
     */
    public function getStatus() : bool
    {
        return $this->state->getStatus();
    }

    /**
     * Получить объект состояния текущей валидации
     * @return State
     */
    public function getState() : State
    {
        return $this->state;
    }

    /**
     * Получить исправленное значение
     * @return mixed
     */
    public function getFixedValue()
    {
        return $this->fixedValue;
    }

    /**
     * Получить флаг исправления ошибок
     * @return bool
     */
    public function getFixErrors() : bool
    {
        return $this->fixErrors;
    }

    /**
     * Получить исходное валидируемое значение
     * @return mixed
     */
    public function getBaseValue()
    {
        return $this->baseValue;
    }

    /**
     * Получить значение для исправления ошибки 'null'
     * @return mixed
     */
    public function getFixNull()
    {
        return $this->fixNull;
    }

    /**
     * Получить значение для исправления ошибки типов
     * @return mixed
     */
    public function getFixType()
    {
        return $this->fixType;
    }

    /**
     * Установить значение для исправления ошибки типов
     * @param $fixType
     * @return $this
     */
    public function setFixType($fixType) : DataType
    {
        $this->fixType = $fixType;
        return $this;
    }

    /**
     * Установить значение для исправления ошибки 'null'
     * @param $fixNull
     * @return $this
     */
    public function setFixNull($fixNull) : DataType
    {
        $this->fixNull = $fixNull;
        return $this;
    }

    /**
     * Установить базовое, валидируемое значение
     * @param mixed $baseValue
     * @return $this
     */
    public function setBaseValue(mixed $baseValue) : DataType
    {
        $this->baseValue = $baseValue;
        return $this;
    }

    /**
     * Установить отвалидированное значение
     * @param $fixedValue
     * @return $this
     */
    public function setFixedValue($fixedValue) : DataType
    {
        $this->fixedValue = $fixedValue;
        return $this;
    }

    /**
     * Установить флаг исправления ошибок
     * @param bool $fixErrors
     * @return $this
     */
    public function setFixErrors(bool $fixErrors) : DataType
    {
        $this->fixErrors = $fixErrors;
        return $this;
    }
}