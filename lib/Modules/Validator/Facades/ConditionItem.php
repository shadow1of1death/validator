<?php

namespace Uniforma\Modules\Validator\Facades;

/**
 * Фасад для быстрой работы с правилами валидации
 */
class ConditionItem
{
    /**
     * Получить объект правила валидации
     * @param string $code - код правила валидации
     * @param array|null $parameters - параметры для валидации
     * @return \Uniforma\Modules\Validator\ConditionItem
     */
    public static function getConditionItem(string $code, ?array $parameters = null) : \Uniforma\Modules\Validator\ConditionItem
    {
        $conditionItem = new \Uniforma\Modules\Validator\ConditionItem();
        return $conditionItem->setCode($code)->setParameters($parameters);
    }
}