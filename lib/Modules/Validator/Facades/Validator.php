<?php

namespace Uniforma\Modules\Validator\Facades;

use Uniforma\Modules\Validator\Collections\ConditionItemCollection;
use Uniforma\Modules\Validator\Validator as BaseValidator;

/**
 * Фасад для быстрой работы с валидатором
 */
class Validator
{

    /**
     * Получить объект валидатора
     * @param mixed $value валидируемое значение
     * @param string $type строковый код объекта типа данных валидации (см <b>Uniforma\Modules\Validator\Utils\DataTypeHelper::getDataTypeList()</b>)
     * @param array $conditions массив правил валидации<br>
     * Пример: ['null', 'type' => ['fixedValueIdNeed'], 'empty']
     * @param bool $fixErrors Необходимо ли исправление ошибок
     * @return BaseValidator
     */
    public static function getValidator(mixed $value, string $type, array $conditions, bool $fixErrors = false) : BaseValidator
    {
        $validator = new BaseValidator();
        $validator->setDataTypeByCode($type);
        $validator->getDataType()->setBaseValue($value);
        if($fixErrors){
            $validator->getDataType()->setFixErrors($fixErrors);
        }
        $conditionItems = new ConditionItemCollection();
        foreach ($conditions as $key => $condition){
            if(is_array($condition)){
                $conditionItems->push(ConditionItem::getConditionItem($key, $condition));
            }
            else{
                $conditionItems->push(ConditionItem::getConditionItem($condition));
            }
        }
        $validator->setConditionItems($conditionItems);
        return $validator;
    }

}