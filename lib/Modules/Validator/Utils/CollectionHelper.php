<?php

namespace Uniforma\Modules\Validator\Utils;

use Uniforma\Modules\Validator\Collections\BaseCollection;

/**
 * Helper для работы с коллекцией
 */
class CollectionHelper
{

    /**
     * Проверить совместимость типа данных итема, с типом данных коллекции
     * @param BaseCollection $collection проверяемая коллекция
     * @param object $item итем, тип данных которого проверяется
     * @return bool
     */
    public static function checkItemType(BaseCollection $collection, object $item) : bool
    {
        if($collection->getType() === null)
            return true;

        if((new \ReflectionClass($item))->getName() === $collection->getType())
            return true;

        return false;
    }
}