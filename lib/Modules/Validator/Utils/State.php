<?php

namespace Uniforma\Modules\Validator\Utils;

/**
 * Класс, хранящий состояние объектов
 */
class State
{
    /**
     * @var bool Флаг состояния (true - ошибок нет, false - возникла ошибка)
     */
    private bool $status = true;

    /**
     * @var string Текст сообщения
     */
    private string $message = '';

    /**
     * Массив даполнительных данных
     * @var array
     */
    public array $data = [];

    /**
     * Очистить состояние (фактически возвращает новый объект)
     * @return State
     */
    public function clear() : State
    {
        return new State();
    }

    /**
     * Получить флаг состояния
     * @return bool
     */
    public function getStatus() : bool
    {
        return $this->status;
    }

    /**
     * Получить текст сообщения
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * Установить ошибку
     * @param string $message текст сообщения
     * @param bool $pushInData добавить текст сообщения в массив дополнительных данных, под ключ 'messagesHistory'
     * @return $this
     */
    public function setError(string $message = '', bool $pushInData = false) : State
    {
        $this->status = false;
        $this->message = $message;
        if($pushInData){
            $this->data['messagesHistory'][] = $message;
        }
        return $this;
    }

    /**
     * Установить сообщение
     * @param string $message
     * @return $this
     */
    public function setMessage(string $message) : State
    {
        $this->message = $message;
        return $this;
    }
}