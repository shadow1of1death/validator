<?php

namespace Uniforma\Modules\Validator\Utils;

use Uniforma\Modules\Validator\DataType;
use Uniforma\Modules\Validator\DataTypes\ArrayValidator;
use Uniforma\Modules\Validator\DataTypes\StringValidator;

/**
 * Helper для работы с объектом валидации определенного типа данных
 */
class DataTypeHelper
{

    /**
     * Получить объект валидации типа данных по строковому коду
     * Набор типов данных определен в методе getDataTypeList, текущего класса
     * @param string $dataType - строковый код типа данных
     * @return DataType|null
     */
    public static function getByCode(string $dataType) : ?DataType
    {
        return self::getDataTypeList()[$dataType];
    }

    /**
     * Получить ассоциативный массив кодов и объектов валидации типов данных
     * @return array
     */
    public static function getDataTypeList() : array
    {
        return [
            'array' => new ArrayValidator(),
            'string' => new StringValidator()
        ];
    }

    /**
     * Метод проверки на 'null'<br>
     * Используется объектами типов данных, так как фактически проверка на 'null' у всех одинаковая
     * @param DataType $dataType объект валидации типа данных
     * @param string $errorMessage сообщение об ошибке, которое будет установлено в объект состояния валидации объекта,
     * в случае если значение объекта является null
     * @return void
     */
    public static function checkNull(DataType &$dataType, string $errorMessage) : void
    {
        if($dataType->getState()->getStatus() || $dataType->getFixErrors()){

            if($dataType->getBaseValue() === null){

                $dataType->getState()->setError($errorMessage, true);
                self::fixErrors($dataType, $dataType->getFixNull());
            }

        }
    }

    /**
     * Установить исправленное значение, если это необходимо
     * @param DataType $dataType объект волидации типа данных
     * @param mixed $fixValue значение для исправления
     * @return void
     */
    public static function fixErrors(DataType &$dataType, mixed $fixValue) : void
    {
        if($dataType->getFixErrors()){
            $dataType->setFixedValue($fixValue);
        }
    }
}