<?php

namespace Uniforma\Modules\Validator\Interfaces;

use Uniforma\Modules\Validator\Collections\BaseCollection;

/**
 * Интерфейс для коллекций
 */
interface CollectionInterface
{

    /**
     * Добавить итем в конец коллекции
     * @param $item
     * @return BaseCollection
     */
    public function push($item) : BaseCollection;

    /**
     * Добавить итем в коллекцию по ключу
     * @param $item
     * @param $key
     * @return BaseCollection
     */
    public function add($item, $key) : BaseCollection;

    /**
     * Получить последний итем коллекции
     * @return mixed
     */
    public function last() : mixed;

    /**
     * Получить первый итем коллекции
     * @return mixed
     */
    public function first() : mixed;

    /**
     * Получить тип данных коллекции
     * @return string|null
     */
    public function getType() : ?string;
}